# /* Public domain. */

O=alloc.o buffer.o buffer_0.o buffer_1.o buffer_2.o buffer_get.o buffer_put.o buffer_read.o buffer_write.o byte_copy.o byte_cr.o byte_diff.o cdb.o cdb_hash.o cdb_make.o error.o error_str.o fmt_ulong.o open_trunc.o scan_ulong.o seek_set.o str_len.o strerr_die.o strerr_sys.o uint32_pack.o uint32_unpack.o
H=uint32.h
A=cdb.a
P=cdbmake cdbdump cdbget cdbstats

CC=gcc
CFLAGS=-Wall -O2 -g

all: $(P)
$(P): $(A)
cdb.o cdb_hash.o cdb_make.o cdbmake.o cdbstats.o cdbdump.o uint32_pack.o unint32_unpack.o: $(H)

cdb.a: $(O)
	ar cr cdb.a $(O)
	ranlib cdb.a || :

uint32.h: tryulong32.c uint32.h1 uint32.h2
	( ($(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o tryulong tryulong.c && ./tryulong) >/dev/null 2>&1 \
	&& cat uint32.h2 || cat uint32.h1) >uint32.h
	rm -f tryulong

check: $(P)
	./check-local $(P)

clean:
	rm -f $(A) $(O) $(H) $(P) *.local
	for i in $(P); do rm -f $${i}.o; done
