/* Public domain. */

#include <unistd.h>
#include "buffer.h"

ssize_t buffer_unixread(int fd,void *buf,size_t len)
{
  return read(fd,buf,len);
}
