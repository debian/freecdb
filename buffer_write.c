/* Public domain. */

#include <unistd.h>
#include "buffer.h"

ssize_t buffer_unixwrite(int fd,const void *buf,size_t len)
{
  return write(fd,buf,len);
}
